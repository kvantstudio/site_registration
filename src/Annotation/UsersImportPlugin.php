<?php

namespace Drupal\site_registration\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotations for UsersImportPlugin.
 *
 * @Annotation
 */
class UsersImportPlugin extends Plugin {

  /**
   * The plugin ID.
   */
  public $id;

  /**
   * Label will be used in interface.
   */
  public $label;

}
