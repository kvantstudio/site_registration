<?php

/**
 * @file
 * Builds placeholder replacement tokens for user-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function site_registration_token_info() {

  $info['tokens']['user']['one-time-profile-activation-url'] = array(
    'name' => t('One-time profile activation URL'),
    'description' => t('The URL of the one-time activation page for the user account.'),
    'restricted' => TRUE,
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function site_registration_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'user' && !empty($data['user'])) {
    $user = $data['user'];
    $account = User::load($user->id());
    foreach ($tokens as $name => $original) {
      switch ($name) {
      case 'one-time-profile-activation-url':
        $replacements[$original] = site_registration_activation_url($account);
        break;
      }
    }
  }

  return $replacements;
}
