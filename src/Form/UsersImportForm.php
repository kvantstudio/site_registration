<?php

namespace Drupal\site_registration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\site_registration\UsersImportBatch;

/**
 * Class UsersImportForm.
 */
class UsersImportForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginList() {
    $definitions = \Drupal::service('plugin.manager.site_registration')->getDefinitions();
    $plugin_list = [];
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $this->t($plugin['label']->render());
    }
    return $plugin_list;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['site_registration.user_import_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_registration_user_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_registration.user_import_settings');

    $form['import_plugin'] = [
      '#title' => $this->t('Select the plugin for importing users'),
      '#type' => 'select',
      '#options' => $this->getPluginList(),
      '#empty_option' => $this->t('None'),
      '#element_validate' => [
        [$this, 'validateImportPlugin'],
      ],
      '#default_value' => $config->get('import_plugin') ? [$config->get('import_plugin')] : '',
    ];

    $form['file'] = [
      '#title' => $this->t('CSV file'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://import/excel',
      '#default_value' => $config->get('fid') ? [$config->get('fid')] : NULL,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
    ];

    // Если файл был загружен.
    if (!empty($config->get('fid'))) {
      $file = File::load($config->get('fid'));
      $created = \Drupal::service('date.formatter')->format($file->created->value, 'custom', 'd.m.Y - H:i:s');

      $form['file_information'] = [
        '#markup' => $this->t('This file was uploaded at @created.', ['@created' => $created]),
      ];

      // Кнопка импорта со своим собственным обработчиком.
      $form['actions']['start_import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Start import'),
        '#submit' => ['::startImport'],
        '#weight' => 100,
      ];
    }

    $form['additional_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional settings'),
    ];

    $form['additional_settings']['skip_first_line'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip first line'),
      '#default_value' => $config->get('skip_first_line'),
      '#description' => $this->t('If file contain titles, this checkbox help to skip first line.'),
    ];

    $form['additional_settings']['delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter'),
      '#default_value' => $config->get('delimiter'),
      '#required' => TRUE,
    ];

    $form['additional_settings']['enclosure'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enclosure'),
      '#default_value' => $config->get('enclosure'),
      '#required' => TRUE,
    ];

    $form['additional_settings']['chunk_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Chunk size'),
      '#default_value' => $config->get('chunk_size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateImportPlugin(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] == 'start_import') {
      $plugin_id = $form_state->getValue('import_plugin');
      if ($plugin_id == "") {
        $form_state->setErrorByName('import_plugin', $this->t('You did not choose the plugin to import.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getTriggeringElement()['#name'] == 'start_import') {
      $plugin_id = $form_state->getValue('import_plugin');
      if ($plugin_id == "") {
        $form_state->setErrorByName('import_plugin', $this->t('You did not choose the plugin to import.'));
      }

      if ($form_state->getValue('chunk_size') < 1) {
        $form_state->setErrorByName('chunk_size', $this->t('Chunk size must be greater or equal 1.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('site_registration.user_import_settings');

    // Идентификатор плагина.
    $import_plugin = $form_state->getValue('import_plugin');
    $config->set('import_plugin', $import_plugin);

    // Сохраняем идентификатор файла.
    $fid_old = $config->get('fid');
    $fid_form = $form_state->getValue('file')[0];

    // Проверяет отличие текущего файла (ранее загруженного) от нового.
    if (empty($fid_old) || $fid_old != $fid_form) {
      // Удаляет старый файл.
      if (!empty($fid_old)) {
        $previous_file = File::load($fid_old);
        \Drupal::service('file.usage')->delete($previous_file, 'site_registration', 'user_import_form', $previous_file->id());
      }

      // Сохраняет новый файл.
      $new_file = File::load($fid_form);
      $new_file->save();

      // Устанавливает принадлежность файла, чтобы предотвартить его удаление по Cron.
      \Drupal::service('file.usage')->add($new_file, 'site_registration', 'user_import_form', $new_file->id());

      // Сохраняет информацию в конфигурацию.
      $config->set('fid', $fid_form)->set('creation', time());
    }

    $config->set('skip_first_line', $form_state->getValue('skip_first_line'))
      ->set('delimiter', $form_state->getValue('delimiter'))
      ->set('enclosure', $form_state->getValue('enclosure'))
      ->set('chunk_size', $form_state->getValue('chunk_size'))
      ->save();
  }

  /**
   * {@inheritdoc}
   *
   * Импорт из файла.
   */
  public function startImport(array &$form, FormStateInterface $form_state) {
    if (!$form_state->hasAnyErrors()) {
      $config = $this->config('site_registration.user_import_settings');

      $fid = $config->get('fid');
      $skip_first_line = $config->get('skip_first_line');
      $delimiter = $config->get('delimiter');
      $enclosure = $config->get('enclosure');
      $chunk_size = $config->get('chunk_size');

      $plugin_id = $form_state->getValue('import_plugin');
      if ($plugin_id != "") {
        $import = new UsersImportBatch($plugin_id, $fid, $skip_first_line, $delimiter, $enclosure, $chunk_size);
        $import->setBatch();
      }
    }
  }
}
