<?php

namespace Drupal\site_registration;

use Drupal\Component\Plugin\PluginBase;

abstract class UsersImportPluginBase extends PluginBase implements UsersImportPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data, &$context) {
    $context['results'][] = '';
    $context['message'] = '';
  }

}
