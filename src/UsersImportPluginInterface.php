<?php

namespace Drupal\site_registration;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface UsersImportPluginInterface extends PluginInspectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getId();

  /**
   * {@inheritdoc}
   */
  public function getLabel();

  /**
   * {@inheritdoc}
   */
  public function processItem($data, &$context);

}
