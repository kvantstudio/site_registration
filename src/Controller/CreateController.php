<?php

/**
 * @file
 * Contains \Drupal\site_registration\Controller\CreateController.
 */

namespace Drupal\site_registration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;

/**
 * Create user controller class.
 */
class CreateController extends ControllerBase {

  /**
   * Реализует проверку регистрации пользователя по логину (e-mail или номер телефона).
   * @param string $name
   *   E-mail или номер телефона.
   * @return \Drupal\user\UserInterface $account || bool FALSE
   */
  public function checkUserByName(string $name) {
    $result = $this->entityTypeManager()->getStorage('user')->loadByProperties([
      'name' => $name
    ]);
    $account = reset($result);
    if ($account instanceof User) {
      return $account;
    }

    return FALSE;
  }

  /**
   *
   * Реализует создание учетной записи.
   * @param string $mail
   *   E-mail, на который будет зарегистрирована учетная запись.
   * @param array $data
   *   Массив с дополнительными данными учетной записи в формате [НАЗВАНИЕ_ПОЛЯ_СУЩНОСТИ => ЗНАЧЕНИЕ].
   *   Если в качестве логина используется номер телефона, обязательно должен быть ключ
   *   $data['field_phone_mobile'] в формате 10. Например, 922xxxxxxx.
   * @param bool $user_login_finalize
   *   Разрешает автоматическую авторизацию учетной записи на сайте сразу после создания.
   * @return \Drupal\user\UserInterface $account
   */
  public function createUser(string $mail, array $data = [], bool $user_login_finalize = FALSE) {
    // Загружаем конфигурацию.
    $config = $this->config('site_registration.settings');

    // Инициализация создания учетной записи.
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $account = User::create();
    $account->enforceIsNew();

    // Generate user password.
    $password = \Drupal::service('password_generator')->generate();
    $account->setPassword($password);

    $account->setEmail($mail);
    $account->set("init", $mail);
    $account->set("langcode", $langcode);
    $account->set("preferred_langcode", $langcode);
    $account->set("preferred_admin_langcode", $langcode);
    $account->set("created", \Drupal::time()->getRequestTime());

    // Определяем, что использовать в качестве логина.
    $registration_type = $config->get('registration_type');
    if (!empty($data['field_phone_mobile']) && $registration_type == 'phone') {
      $phone = trim($data['field_phone_mobile']);
      $account->setUsername($phone);
    } else {
      $account->setUsername($mail);
    }

    // Определяем блокировать или нет учетную запись при создании.
    $status = 1;
    $blocking_user_account = $config->get('blocking_user_account');
    if ($blocking_user_account) {
      $status = 0;
    }
    $account->set("status", $status);

    // Устанавливаем значения дополнительных полей.
    foreach ($data as $key => $value) {
      if ($account->hasField($key)) {
        $account->set($key, $value);
      }
    }

    $account->save();

    // Активация и авторизация пользователя.
    if ($user_login_finalize) {
      $account->set('status', 1);
      $account->save();

      user_login_finalize($account);
    } else {
      // Отправляем уведомление на e-mail о создании учетной записи.
      _user_mail_notify('register_no_approval_required', $account);
    }

    return $account;
  }
}
