<?php

/**
 * @file
 * Contains \Drupal\site_registration\Form\SiteRegistrationSettingsForm
 */

namespace Drupal\site_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Subscription settings form.
 */
class SiteRegistrationSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_registration.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_registration_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Текст сообщения над e-mail на форме регистрации.
    $form['email_notification_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The text on the registration page of the account'),
      '#description' => '',
      '#rows' => 2,
      '#default_value' => $config->get('email_notification_text') ? $config->get('email_notification_text') : $this->t("For full access to the website you must be registered."),
    ];

    // Текст на странице уведомления об успешной регистрации на сайте.
    $form['successful_registration_notification_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The text on the page notification of successful registration to the website'),
      '#description' => '',
      '#rows' => 2,
      '#default_value' => $config->get('successful_registration_notification_text') ? $config->get('successful_registration_notification_text') : $this->t("Congratulations on your successful registration! Details of access to the website have been sent to your e-mail."),
    ];

    // Текст на странице уведомления о подтверждении регистрации на сайте.
    $form['confirm_registration_notification_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The text of the notification of confirmation of registration on the website'),
      '#description' => '',
      '#rows' => 2,
      '#default_value' => $config->get('confirm_registration_notification_text') ? $config->get('confirm_registration_notification_text') : $this->t("Your registration has been successfully confirmed."),
    ];

    // Разрешить авторизацию только по E-mail.
    $form['authorization_only_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow authorization only by E-mail'),
      '#default_value' => $config->get('authorization_only_mail'),
      '#description' => $this->t('Select the option if you want to allow user authorization only by email address. Logins that are not the correct E-mail address will be banned during authorization.'),
    ];

    // Разрешить блокировку учетной записи при создании.
    $form['blocking_user_account'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow account blocking during creation'),
      '#default_value' => $config->get('blocking_user_account'),
    ];

    $form['registration_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of registration'),
      '#options' => [
        'mail' => $this->t('E-mail'),
        'phone' => $this->t('Phone'),
      ],
      '#default_value' => $config->get('registration_type'),
      '#description' => $this->t('Select the parameter that will be used as a username when registering and authorizing the user.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Записывает значения в конфигурацию.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $config->set('email_notification_text', trim($form_state->getValue('email_notification_text')));
    $config->set('successful_registration_notification_text', trim($form_state->getValue('successful_registration_notification_text')));
    $config->set('confirm_registration_notification_text', trim($form_state->getValue('confirm_registration_notification_text')));
    $config->set('authorization_only_mail', (int) $form_state->getValue('authorization_only_mail'));
    $config->set('blocking_user_account', (int) $form_state->getValue('blocking_user_account'));
    $config->set('registration_type', $form_state->getValue('registration_type'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
