<?php

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\user\UserInterface;

/**
 * Implements hook_ENTITY_TYPE_insert() for user entities.
 */
function site_registration_user_insert(UserInterface $account) {
  // Don't create a new username if one is already set.
  $name = $account->getDisplayName();
  if (!empty($name) && strpos($name, 'site_registration_') !== 0) {
    return;
  }

  // Other modules may implement hook_site_registration_name($edit, $account)
  // to generate a username (return a string to be used as the username, NULL
  // to have site_registration generate it).
  $names = \Drupal::moduleHandler()->invokeAll('site_registration_name', [$account]);

  // Remove any empty entries.
  $names = array_filter($names);
  if (empty($names)) {
    $name = $account->getEmail();
  } else {
    // One would expect a single implementation of the hook, but if there
    // are multiples out there use the last one.
    $name = array_pop($names);
  }

  // @todo Make sure we still need this update.
  // Replace with generated username.
  $status = 1;
  $config = \Drupal::config('site_registration.settings');
  $blocking_user_account = $config->get('blocking_user_account');
  if ($blocking_user_account) {
    $status = 0;
  }

  $account->setUsername($name);
  $account->set('status', $status);
  $account->save();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_registration_form_user_register_form_alter(&$form, FormStateInterface $form_state) {
  $config = \Drupal::config('site_registration.settings');
  $email_notification_text = $config->get('email_notification_text');
  if (!empty($email_notification_text)) {
    $form['account']['mail']['#prefix'] = '<div class="user-register-form__description">' . $email_notification_text . '</div>';
  }

  $password = \Drupal::service('password_generator')->generate(10);

  $form['account']['name']['#type'] = 'value';
  $form['account']['name']['#value'] = 'site_registration_' . $password;
  $form['account']['mail']['#title'] = t('E-mail address');
  $form['account']['mail']['#description'] = "";
  $form['actions']['submit']['#submit'][] = 'site_registration_user_register_submit_handler';
}

/**
 * Implements hook_user_register_submit_handler().
 */
function site_registration_user_register_submit_handler($form, &$form_state) {
  $form_state->setRedirect('site_registration.successful_registration');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_registration_form_user_pass_alter(&$form, FormStateInterface $form_state) {
  $form['name']['#title'] = t('E-mail address');
  $form['name']['#type'] = 'email';
  $form['name']['#maxlength'] = Email::EMAIL_MAX_LENGTH;

  $form['mail']['#markup'] = "";
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_registration_form_user_login_form_alter(&$form, FormStateInterface $form_state) {
  $config = \Drupal::service('config.factory')->get('site_registration.settings');
  if ($config->get('authorization_only_mail')) {
    $form['name']['#type'] = 'email';
    $form['name']['#title'] = t('E-mail address');
    $form['name']['#maxlength'] = Email::EMAIL_MAX_LENGTH;
    $form['name']['#element_validate'][] = 'site_registration_user_login_validate';
  }

  $form['name']['#description'] = '';
  $form['pass']['#description'] = '';
}

/**
 * Form element validation handler for the user login form.
 * Allows users to authenticate by email, which is our preferred method.
 */
function site_registration_user_login_validate($form, FormStateInterface $form_state) {
  $mail = $form_state->getValue('name');
  if (!empty($mail) && ($user = user_load_by_mail($mail))) {
    // Keep the email value in form state for further validation.
    $form_state->setValue('name', $user->getDisplayName());
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_registration_form_user_form_alter(&$form, FormStateInterface $form_state) {
  $form['account']['name']['#title'] = t('Display name');
}

/**
 * Function to clean up username.
 *
 * e.g.
 *     Replace two or more spaces with a single underscore
 *     Strip illegal characters.
 *
 * @param string $name
 *   The username to be cleaned up.
 *
 * @return string
 *   Cleaned up username.
 */
function site_registration_cleanup_username($name) {
  // Strip illegal characters.
  $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);

  // Strip leading and trailing spaces.
  $name = trim($name);

  // Convert any other series of spaces to a single underscore.
  $name = preg_replace('/  +/', '_', $name);

  // If there's nothing left use a default.
  $name = ('' === $name) ? t('user') : $name;

  // Truncate to a reasonable size.
  $name = (mb_strlen($name) > UserInterface::USERNAME_MAX_LENGTH) ? mb_substr($name, 0, UserInterface::USERNAME_MAX_LENGTH) : $name;

  return $name;
}

/**
 * Generates a unique URL for a user to login.
 *
 * @param object $account
 *   An object containing the user account, which must contain at least the
 *   following properties:
 *   - uid: The user ID number.
 *   - login: The UNIX timestamp of the user's last login.
 * @return \Drupal\Core\GeneratedUrl|string A unique URL that provides a one-time log in for the user, from which
 * A unique URL that provides a one-time log in for the user, from which
 * they can change their password.
 */
function site_registration_activation_url($account) {
  $request_time = \Drupal::time()->getRequestTime();
  $url = Url::fromRoute('site_registration.profile_activation',
    [
      'uid' => $account->id(),
      'timestamp' => $request_time,
      'hash' => user_pass_rehash($account, $request_time),
    ],
    [
      'absolute' => TRUE,
    ]
  );

  return $url->toString();
}

/**
 * Implements hook_theme().
 */
function site_registration_theme($existing, $type, $theme, $path) {
  return [
    'site_registration_mail_get_password' => [
      'variables' => array('email' => NULL, 'password' => NULL),
      'template' => 'site-registration-mail-get-password',
    ],
    'site_registration_successful_registration' => [
      'variables' => array(),
      'template' => 'site-registration-successful-registration',
    ],
    'site_registration_confirm_registration' => [
      'variables' => array(),
      'template' => 'site-registration-confirm-registration',
    ],
  ];
}

/**
 * Implements template_preprocess_THEME_FUNCTION().
 */
function template_preprocess_site_registration_mail_get_password(&$variables) {
  global $base_url;
  $variables['base_url'] = $base_url;
  $variables['path_to_module'] = \Drupal::service('extension.list.module')->getPath('site_registration');

  $config = \Drupal::config('site_registration.settings');
  $variables['letter_notification_text'] = $config->get('letter_notification_text');
}

/**
 * Implements template_preprocess_THEME_FUNCTION().
 */
function template_preprocess_site_registration_successful_registration(&$variables) {
  global $base_url;
  $variables['base_url'] = $base_url;
  $variables['path_to_module'] = \Drupal::service('extension.list.module')->getPath('site_registration');

  $config = \Drupal::config('site_registration.settings');
  $variables['successful_registration_notification_text'] = $config->get('successful_registration_notification_text');
}

/**
 * Implements template_preprocess_THEME_FUNCTION().
 */
function template_preprocess_site_registration_confirm_registration(&$variables) {
  global $base_url;
  $variables['base_url'] = $base_url;
  $variables['path_to_module'] = \Drupal::service('extension.list.module')->getPath('site_registration');

  $config = \Drupal::config('site_registration.settings');
  $variables['confirm_registration_notification_text'] = $config->get('confirm_registration_notification_text');

  $variables['path_to_account'] = "user.page";
  $module_exist = \Drupal::service('module_handler')->moduleExists('site_account');
  if ($module_exist) {
    $variables['path_to_account'] = "site_account.personal_account";
  }
}

/**
 * Implements hook_form_alter().
 */
function site_registration_form_alter(&$form, $form_state, $form_id) {
  if (in_array($form_id, array('user_admin_settings'))) {
    $config = \Drupal::config('site_registration.settings');
    $form['site_registration_settings'] = [
      '#type' => 'details',
      '#title' => t('Registration by e-mail'),
      '#description' => '',
      '#group' => 'email',
      '#weight' => 10,
    ];
    $form['site_registration_settings']['letter_notification_text'] = [
      '#type' => 'textarea',
      '#title' => t('The text of the message in the letter when sending a login and password to the website'),
      '#default_value' => $config->get('letter_notification_text') ? $config->get('letter_notification_text') : t("Good afternoon! Remember your current login and password to access the website. Later you can change them in your personal account."),
      '#rows' => 3,
    ];

    $form['#submit'][] = 'site_registration_settings_submit';
  }
}

/**
 * Custom submit function for user_admin_settings form_id.
 */
function site_registration_settings_submit(&$form, $form_state) {
  // Записывает значения в конфигурацию.
  $config = \Drupal::service('config.factory')->getEditable('site_registration.settings');
  $config->set('letter_notification_text', trim($form_state->getValue('letter_notification_text')));
  $config->save();
}
