<?php

/**
 * @file
 * Contains \Drupal\site_registration\Controller\SiteRegistrationController.
 */

namespace Drupal\site_registration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

class SiteRegistrationController extends ControllerBase {

  /**
   * Returns a page title.
   */
  public function getTitle(Request $request) {
    $string = $request->query->get('title');
    $string = trim($string);

    if ($string) {
      return $string;
    } else {
      return $this->t('Successful registration');
    }
  }

  /**
   * Returns the user personal area page after account activation.
   *
   * @param int $uid
   *   UID of user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The form structure or a redirect response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the login link is for a blocked user or invalid user ID.
   */
  public function profileActivation(int $uid, int $timestamp, string $hash) {
    $account = User::load($uid);
    if ($account instanceof UserInterface) {
      // Проверка пароля.
      $rehash = user_pass_rehash($account, $timestamp);
      if ($hash == $rehash) {
        // Устанавливаем новый пароль пользователю и активируем учетную запись.
        $password = \Drupal::service('password_generator')->generate();
        $account->setPassword($password);
        $account->set("status", 1);
        $account->save();

        // Формируем сообщение в HTML.
        $renderData = [
          '#theme' => 'site_registration_mail_get_password',
          '#email' => $account->getEmail(),
          '#password' => $password,
        ];
        $message = \Drupal::service('renderer')->render($renderData, FALSE);

        // Отправка сообщения на почту.
        $data_mail = [
          'to' => $account->getEmail(),
          'subject' => $this->t('Your access details'),
          'message' => $message,
          'attachments' => [],
        ];
        kvantstudio_mail_send($data_mail);

        // Завершаем авторизацию пользователя.
        user_login_finalize($account);
      }

      // Редирект на страницу подтверждения регистрации.
      return $this->redirect('site_registration.confirm_registration');
    } else {
      return $this->redirect('<front>');
    }
  }

  /**
   * Страница уведомление о успешной регистрации.
   */
  public function successfulRegistration() {
    return array(
      '#type' => 'container',
      '#theme' => 'site_registration_successful_registration',
    );
  }

  /**
   * Страница уведомление подтверждения регистрации.
   */
  public function confirmRegistration() {
    return array(
      '#type' => 'container',
      '#theme' => 'site_registration_confirm_registration',
    );
  }

}
