<?php

namespace Drupal\site_registration;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an UsersImport plugin manager.
 */
class UsersImportPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteRegistration',
      $namespaces,
      $module_handler,
      'Drupal\site_registration\UsersImportPluginInterface',
      'Drupal\site_registration\Annotation\UsersImportPlugin'
    );

    # hook_site_registration_info_alter();
    $this->alterInfo('site_registration_user_import_plugin_info');
    $this->setCacheBackend($cache_backend, 'site_registration_user_import_plugin');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}
